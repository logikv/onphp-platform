<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.03.17
 * Time: 11:08
 */
class HtmlToPdfController extends ProjectBotBaseController
{

    public function htmlToPdfAction(HttpRequest $httpRequest)
    {
        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $html = null;
        if (!array_key_exists('html', $httpRequest->getPost()))
            return (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('error', 'No html data!!!')
                        ->set('success', false)
                )
                ->setView((new JsonView()));

        if (trim($httpRequest->getPostVar('html')) == '')
            return (new ModelAndView())
                ->setModel((new Model())
                    ->set('error', 'html data zero!!!')
                    ->set('success', false)
                )
                ->setView(new JsonView());

        $fileName = TimestampTZ::makeNow()->toFormatString('YmdHis');
        error_reporting(0);
        $mpdf = new mPDF('UTF-8', 'A4', '', '', 32, 25, 27, 25, 16, 13);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML(html_entity_decode($httpRequest->getPostVar('html')));
        $mpdf->Output(PATH_WEB . 'pdf/' . $fileName . '.pdf');

        $base64pdf = base64_encode(file_get_contents(PATH_WEB . 'pdf/' . $fileName . '.pdf'));

        return (new ModelAndView())
            ->setModel((new Model())->set('base64', $base64pdf))
            ->setView(new JsonView());
    }

    public function exampleAction(HttpRequest $httpRequest)
    {
        return (new ModelAndView())
            ->setModel((new Model()))
            ->setView('html-to-pdf/example');
    }

    public function getMapping()
    {
        return [
            'htmlToPdf' => 'htmlToPdfAction',
            'example' => 'exampleAction'
        ];
    }
}