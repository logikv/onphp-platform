<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.10.16
 * Time: 16:02
 */
class UploadController extends ProjectServiceController
{
    public $form;

    public function indexView(HttpRequest $httpRequest)
    {

        print_r(__FILE__); die;

        $this->form =
            (new Form())->set(
                Primitive::file('file')
                    ->addAllowedMimeType('video/mp4')
                    ->required()
            )
                ->import($httpRequest->getFiles());

        print_r($httpRequest->getPost());

        $uploadAt = TimestampTZ::makeNow();
        $mimeType = MimeType::getByMimeType($this->form->get('file')->getRawValue()['type']);
        $fileName = $uploadAt->toStamp() . '.' . $mimeType->getExtension();

        FileUtils::upload(
            $this->form->get('file')->getRawValue()['tmp_name'],
            \Helpers\Uploader\TmpStore::create()->getPath() . 'video/' . $fileName
        );

        die;
    }


    /**
     * Маппинг
     *
     * @return array
     */
    protected function getMapping()
    {
        return [
            'index' => 'indexView',
        ];
    }

    /**
     * Доступные методы без авторизации
     *
     * @return array
     */
    public function getFreeAction()
    {
        return [
            'index'
        ];
    }
}