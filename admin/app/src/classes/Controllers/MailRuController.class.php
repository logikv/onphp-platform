<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 26.04.16
 * Time: 15:13
 */
class MailRuController extends PlatformBasicMethodMappedController
{

    private $siteId = 740551;
    private $token = '30a0a1ce750f729518d869ffe3290e1a';
    private $uIdGroup = 4277108916468699271;

    public function indexAction(HttpRequest $httpRequest)
    {
        return
            (new ModelAndView())
                ->setModel((new Model()))
                ->setView('mail.ru/receiver');
    }

    public function testAction(HttpRequest $request)
    {

        $array = [
            'client_id' => $this->siteId,
            'response_type' => 'code_and_token',
            'redirect_uri' => urldecode('http://social.newsteam.ru/mail-callback'),
            'scope' => 'photos guestbook stream messages events'
        ];

        $url = 'https://connect.mail.ru/oauth/authorize?';

        foreach ($array as $item => $value) {
            $url .= "$item=$value&";
        }

        echo "<a href='$url'>Авторизация через майлру</a>";
        die;
    }

    public function callbackAction(HttpRequest $request)
    {

        if (!$request->getGet()) {

            $out = <<<TYPEOTHER
            <script type="text/javascript">
var url = location.href;
location.href = url.replace('#', '?');
            </script>

TYPEOTHER;


            echo $out;

        } else {
            echo "<pre>";

            $array = [
                'app_id' => $this->siteId,
                'session_key' => $request->getGetVar('access_token'),
                'uids' => $request->getGetVar('x_mailru_vid'),
                'method' => 'users.getInfo',
                'secure' => 1
            ];


            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => 'http://www.appsmail.ru/platform/api?method=users.getInfo&app_id=' .
                    $this->siteId . '&session_key=' .
                    $request->getGetVar('access_token') . '&sig=' .
                    $this->signServerServer($array, $this->token) . '&uids=' . $request->getGetVar('x_mailru_vid') . '&secure=1',
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
            ));

            $result = curl_exec($curl);

            curl_close($curl);
            echo "<pre>";
            print_r(json_decode($result, true));
            /**
             * http://www.appsmail.ru/platform/api?method=stream.post&app_id=123456&
             * session_key=551fd0e4779e35859dfccd03397dc8a0&
             * sig=95a393ed7639184a52f7a17b6771a92&text=test
             */

            /**
            http://www.appsmail.ru/platform/api?method=multipost.send&text=test&
            app_id=423004&session_key=551fd0e4779e35859dfccd03397dc8a0&
            sig=8730beefc28cb1e6d294aa3f9608efe4
             */

            $array = [
                'method' => 'stream.share',
                'uid2' => $this->uIdGroup,
                'app_id' => $this->siteId,
                'session_key' => $request->getGetVar('access_token'),
                'url' => urlencode('http://www.pravda.ru/news/society/27-04-2016/1299657-kalgina-0/'),
                'secure' => 1
            ];

            $url = 'http://www.appsmail.ru/platform/api?';

            foreach ($array as $key => $value) {
                $url .= "$key=$value&";
            }

            $url .= "sig=".$this->signServerServer($array, $this->token);

            var_dump($url);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
            ));

            $result = curl_exec($curl);

            curl_close($curl);
            echo "<pre>";
            var_dump($result);


            die;

        }

        die;
    }


    function signServerServer(array $requestParams, $secretKey)
    {
        ksort($requestParams);
        $params = '';
        foreach ($requestParams as $key => $value) {
            $params .= "$key=$value";
        }
        return md5($params . $secretKey);
    }

    /**
     * Мапинг методов конроллера
     * Можно вернуть пустой массив если брать с учетом
     * что экшен будет тот который прописан в роут конфиге
     *
     * @return array
     */
    protected function getMapping()
    {
        return [
            'index' => 'indexAction',
            'test' => 'testAction',
            'callback' => 'callbackAction'
        ];
    }
}