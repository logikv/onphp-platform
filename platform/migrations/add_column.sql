ALTER TABLE bots.referrers_links ADD commercial_link BOOLEAN DEFAULT false;

-- object: bots.plan_for_commercial_materials | type: TABLE --
-- DROP TABLE bots.plan_for_commercial_materials;
CREATE TABLE bots.plan_for_commercial_materials(
  id bigserial,
  referrer_link_id bigint,
  date timestamptz,
  count integer,
  CONSTRAINT pk__plan_for_commercial_materials PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__referrers_link | type: CONSTRAINT --
-- ALTER TABLE bots.plan_for_commercial_materials DROP CONSTRAINT fk__referrers_link;
ALTER TABLE bots.plan_for_commercial_materials ADD CONSTRAINT fk__referrers_link FOREIGN KEY (referrer_link_id)
REFERENCES bots.referrers_links (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
