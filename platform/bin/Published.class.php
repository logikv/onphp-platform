<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.01.16
 * Time: 17:18
 */
include('bin_config.inc.php');

class Published
{
    /** @var TextOutput */
    private $log;

    public function __construct()
    {
        $this->log = new TextOutput();
        $this->published();
    }

    /**
     * published link everyWhere
     */
    public function published()
    {
        $this->log->writeErrLine(__FUNCTION__);
        $links = (new PlatformSocialPublishedLink())
            ->dao()->getNotPublishedAndEverywhere(20);

        foreach ($links as $link) {
            try {
                $this->log->writeErrLine($link->getLinkUrl());
                $this->inPage($link);
                $this->inGroup($link);
            } catch (ServiceException $e) {
                $this->log->writeErrLine($e->getMessage());
            } catch (Exception $e) {
                $this->log->writeErrLine($e->getMessage());
            }
        }

        $expectsLinks = (new PlatformSocialPublishedLink())
            ->dao()->getExpectsLinks(TimestampTZ::makeNow());

        foreach ($expectsLinks as $link) {
            try {
                $this->log->writeErrLine($link->getLinkUrl());
                if (!is_null($link->getAppAdminGroupId())) {
                    $app = $link->getAppAdminGroup()->getAppAdmin()->getApp();

                    if ($app->getSocialNetwork()->getId() == PlatformSocialNameEnum::FACEBOOK)
                    {
                        $this->log->writeErrLine($app->getSocialNetwork()->getName());
                        $response = (new PlatformSocialProcessor())->groupPublicationLinkFacebook($link);
                        $this->log->writeErrLine($response->getBody());

                    } elseif ($app->getSocialNetwork()->getId() == PlatformSocialNameEnum::VK) {
                        $this->log->writeErrLine($app->getSocialNetwork()->getName());
                        try {
                            $response = (new PlatformSocialProcessor())->groupPublicationLinkVkontakte($link, true);
                            $this->log->writeErrLine($response->response->postId);
                        } catch (ServiceException $e) {
                            $this->log->writeErrLine($e->getMessage());
                        }
                    }elseif($app->getSocialNetwork()->getId() == PlatformSocialNameEnum::OK)
                    {
                        $this->log->writeErrLine($app->getSocialNetwork()->getName());
                        $response = (new PlatformSocialProcessor())->groupPublicationLinkOk($link,true);
                        $this->log->writeErrLine($response);
                    }
                }

                if(!is_null($link->getAppAdminPageId())) {
                    $app = $link->getAppAdminPage()->getAppAdmin()->getApp();
                    if ($app->getSocialNetwork()->getId() == PlatformSocialNameEnum::FACEBOOK) {
                        $this->log->writeErrLine($app->getSocialNetwork()->getName());
                        $response = (new PlatformSocialProcessor())->pagePublicationLinkFacebook($link, true);
                        $this->log->writeErrLine($response->getBody());
                    }
                }

                sleep(5);
            } catch (ServiceException $e) {
                $this->log->writeErrLine($e->getMessage());
            } catch (Exception $e) {
                $this->log->writeErrLine($e->getMessage());
            }
        }
    }

    /**
     * @param PlatformSocialPublishedLink $link
     * @throws ServiceException
     */
    private function inGroup(PlatformSocialPublishedLink $link)
    {
        foreach ($link->getFlow()->getGroups() as $dimension) {
            $this->log->writeErrLine($dimension->getGroup()->getName());

            $group = $dimension->getGroup();
            $admin = $group->getAppAdmin();
            $app = $admin->getApp();

            $link->setAppAdminGroup($dimension->getGroup());

            if ((int)$app->getSocialNetwork()->getId() == PlatformSocialNameEnum::FACEBOOK) {
                $this->log->writeErrLine($app->getSocialNetwork()->getName());
                $response = (new PlatformSocialProcessor())->groupPublicationLinkFacebook($link, true);
                $this->log->writeErrLine($response->getBody());
            }

            if ((int)$app->getSocialNetwork()->getId() == PlatformSocialNameEnum::VK) {
                $this->log->writeErrLine($app->getSocialNetwork()->getName());
                try {
                    $response = (new PlatformSocialProcessor())->groupPublicationLinkVkontakte($link, true);
                    $this->log->writeErrLine($response->response->postId);
                } catch (ServiceException $e) {
                    $this->log->writeErrLine($e->getMessage());
                }
            }

            if((int)$app->getSocialNetwork()->getId() == PlatformSocialNameEnum::OK)
            {
                $this->log->writeErrLine($app->getSocialNetwork()->getName());
                $response = (new PlatformSocialProcessor())->groupPublicationLinkOk($link,true);
                $this->log->writeErrLine($response);
            }
            sleep(5);
        }
    }

    /**
     * @param PlatformSocialPublishedLink $link
     * @throws ServiceException
     */
    private function inPage(PlatformSocialPublishedLink $link)
    {
        foreach ($link->getFlow()->getPages() as $dimension) {
            $page = $dimension->getPage();
            $admin = $page->getAppAdmin();
            $app = $admin->getApp();

            $link->setAppAdminPage($dimension->getPage());

            if ((int)$app->getSocialNetwork()->getId() == PlatformSocialNameEnum::FACEBOOK) {
                $this->log->writeErrLine($app->getSocialNetwork()->getName());
                $response = (new PlatformSocialProcessor())->pagePublicationLinkFacebook($link, true);
                $this->log->writeErrLine($response->getBody());
            }
            sleep(5);
        }
    }
}

(new TextOutput())
    ->writeErrLine('Start cron ' . TimestampTZ::makeNow()->toFormatString('m.d.Y H:i:s'));

new Published();

(new TextOutput())
    ->writeErrLine('Stop cron ' . TimestampTZ::makeNow()->toFormatString('m.d.Y H:i:s'));