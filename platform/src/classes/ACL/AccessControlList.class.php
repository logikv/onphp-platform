<?php

namespace ACL;
{
    class AccessControlList
    {
        /**
         * @param AccessToResources $accessToResources
         */
        public function setResources(AccessToResources $accessToResources)
        {

        }

        /**
         * @param AccessRoles $accessRoles
         */
        public function setAccessRoles(AccessRoles $accessRoles)
        {

        }

        /**
         * @param AccessPrivileges $accessPrivileges
         */
        public function setPrivilege(AccessPrivileges $accessPrivileges)
        {

        }
    }
}