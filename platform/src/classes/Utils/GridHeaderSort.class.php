<?php

/**
 * Class GridHeaderSort
 */
class GridHeaderSort implements IGridHeaderWidget
{
    /**
     * @var
     */
    protected $field;
    /**
     * @var null|string
     */
    protected $label;

    /**
     * @param $field
     * @param null $label
     */
    public function __construct($field, $label = null)
    {
        $this->field = $field;

        $label = is_null($label) ? ucfirst($field) : $label;

        $this->label = $label;
    }

    /**
     * @param $field
     * @param null $label
     * @return GridHeaderSort
     */
    public static function me($field, $label = null)
    {
        return new self($field, $label);
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     * @return GridHeaderSort
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param null|string $label
     * @return GridHeaderSort
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->getLinkedField();
    }

    /**
     * @return string
     */
    protected function getLinkedField()
    {
        $request = PlatformRequestHelper::me()->getHttpRequest();
        $route = RouterRewrite::me()->setRequest($request);
        $href = $route->assembly();

        if ($request->hasGetVar($this->field)) {
            $order = $request->getGetVar($this->field);
        } else {
            $order = null;
        }

        if ($order == null) {
            $sort = 'asc';
        } else {
            $sort = $order == 'asc' ? 'desc' : 'asc';
        }

        $getVars = array();

        foreach ($request->getGet() as $get => $direction) {
            if ($get == $this->field) {
                $direction = $sort;
            }

            $getVars[] = $get . '=' . $direction;

        }

        if (empty($getVars)) {
            $getVars[] = $this->field . '=' . $sort;
        }

        $href .= '?' . implode('&', $getVars);

        return '<a href="' . $href . '">' . $this->label . '</a>';
    }

} 