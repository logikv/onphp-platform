<?php

interface IGridHeaderWidget {

    public function render();

    public function getField();

    public function getLabel();
} 