<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19.05.16
 * Time: 12:39
 */
class LinkStatusEnum extends Enum
{
    const
        /** опубликованна */
        PUBLISHED = 1,
        /** ожидает */
        EXPECTS = 2,
        /** ошибка */
        PUBLISHING_ERROR = 3;

    protected static $names = [
        self::PUBLISHED => 'published',
        self::EXPECTS => 'expects',
        self::PUBLISHING_ERROR => 'error'
    ];

    public static function published()
    {
        return new self(self::PUBLISHED);
    }

    public static function excepts()
    {
        return new self(self::EXPECTS);
    }

    public static function publishedError()
    {
        return new self(self::PUBLISHING_ERROR);
    }
}