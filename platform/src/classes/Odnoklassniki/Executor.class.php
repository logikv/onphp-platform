<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.07.16
 * Time: 17:23
 */
namespace Odnoklassniki;

class Executor
{
    public function perform($url, $type="GET", $params=array(), $timeout=30) {
        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            if ($type == "POST") {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12');
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        } else {
            return "{}";
        }
    }

    public static function preparedRequest($array)
    {
        ksort($array);
        $string = "";
        foreach($array as $key=>$val) {
            if (is_array($val)) {
                $string .= $key."=".self::preparedRequest($val);
            } else {
                $string .= $key."=".$val;
            }
        }
        return $string;
    }

}