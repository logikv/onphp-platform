<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15.06.16
 * Time: 12:29
 */

namespace Odnoklassniki;

class Odnoklassniki
{
    private $clientId = null;
    private $clientSecret = null;
    private $applicationKey = null;
    private $redirectUri = null;

    const
        BASE_AUTHORIZATION_URL = 'http://www.odnoklassniki.ru/oauth/authorize';

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param mixed $clientSecret
     * @return $this
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApplicationKey()
    {
        return $this->applicationKey;
    }

    /**
     * @param mixed $applicationKey
     * @return $this
     */
    public function setApplicationKey($applicationKey)
    {
        $this->applicationKey = $applicationKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    /**
     * @param mixed $redirectUri
     * @return $this
     */
    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;
        return $this;
    }

    public function getUrlAuth($params = null)
    {
        $request = null;

        if(is_null($params))
        {
            $params = [
                'client_id' => $this->clientId,
                'scope' => 'GROUP_CONTENT,VALUABLE_ACCESS',
                'response_type' => 'code',
                'redirect_uri' => urlencode($this->redirectUri)
            ];
        }

        foreach ($params as $key => $value)
        {
            $request.= $key.'='.$value.'&';
        }
        return self::BASE_AUTHORIZATION_URL.'?'.$request;
    }


}