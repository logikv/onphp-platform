<?php

/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-01-30 10:55:55                    *
 *   This file will never be generated again - feel free to edit.            *
 *****************************************************************************/
class PlatformBotHistoryInvitedDAO extends AutoPlatformBotHistoryInvitedDAO
{
    /**
     * @return string
     */
    public function getSequence()
    {
        return parent::getSequence().'_seq';
    }

    /**
     * @param $id
     * @param int $expires
     * @return PlatformBotHistoryInvited|mixed
     */
    public function getById($id, $expires = Cache::EXPIRES_MEDIUM)
    {
        return parent::getById($id, $expires);
    }

}

?>