<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2016-10-10 10:06:00                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoProtoSocialPublishedVideo extends AbstractProtoClass
	{
		protected function makePropertyList()
		{
			return array(
				'id' => LightMetaProperty::fill(new LightMetaProperty(), 'id', null, 'integerIdentifier', 'SocialPublishedVideo', 8, true, true, false, null, null),
				'title' => LightMetaProperty::fill(new LightMetaProperty(), 'title', null, 'string', null, 256, true, true, false, null, null),
				'description' => LightMetaProperty::fill(new LightMetaProperty(), 'description', null, 'string', null, 256, true, true, false, null, null),
				'appAdminPage' => LightMetaProperty::fill(new LightMetaProperty(), 'appAdminPage', 'app_admin_page_id', 'identifier', 'PlatformSocialAppAdminPage', null, false, false, false, 1, 3),
				'appAdminGroup' => LightMetaProperty::fill(new LightMetaProperty(), 'appAdminGroup', 'app_admin_group_id', 'identifier', 'PlatformSocialAppAdminGroup', null, false, false, false, 1, 3),
				'publishedAt' => LightMetaProperty::fill(new LightMetaProperty(), 'publishedAt', 'published_at', 'timestampTZ', 'TimestampTZ', null, false, true, false, null, null),
				'publishedId' => LightMetaProperty::fill(new LightMetaProperty(), 'publishedId', 'published_id', 'string', null, 512, false, true, false, null, null),
				'createdAt' => LightMetaProperty::fill(new LightMetaProperty(), 'createdAt', 'created_at', 'timestampTZ', 'TimestampTZ', null, false, true, false, null, null),
				'status' => LightMetaProperty::fill(new LightMetaProperty(), 'status', null, 'enum', 'LinkStatusEnum', null, false, false, false, 1, 3),
				'pathToTheFile' => LightMetaProperty::fill(new LightMetaProperty(), 'pathToTheFile', 'path_to_the_file', 'string', null, 512, false, true, false, null, null)
			);
		}
	}
?>