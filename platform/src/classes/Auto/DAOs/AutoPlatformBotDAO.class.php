<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-01-13 13:10:19                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoPlatformBotDAO extends StorableDAO
	{
		public function getTable()
		{
			return 'bots.bot';
		}
		
		public function getObjectName()
		{
			return 'PlatformBot';
		}
		
		public function getSequence()
		{
			return 'bots.bot_id';
		}
	}
?>