<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-02-27 13:47:00                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoPlatformBotBanDAO extends StorableDAO
	{
		public function getTable()
		{
			return 'bots.bot_ban';
		}
		
		public function getObjectName()
		{
			return 'PlatformBotBan';
		}
		
		public function getSequence()
		{
			return 'bots.bot_ban_id';
		}
	}
?>