<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-01-30 10:55:55                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoPlatformBotHistoryInvitedDAO extends StorableDAO
	{
		public function getTable()
		{
			return 'bots.history_invited';
		}
		
		public function getObjectName()
		{
			return 'PlatformBotHistoryInvited';
		}
		
		public function getSequence()
		{
			return 'bots.history_invited_id';
		}
	}
?>