<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-04-25 08:20:27                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoPlatformVisitedPageCrawlTime extends IdentifiableObject
	{
		protected $bot = null;
		protected $botId = null;
		protected $crawTime = null;
		protected $crawTimeId = null;
		protected $status = null;
		protected $statusId = null;
		
		/**
		 * @return PlatformBot
		**/
		public function getBot()
		{
			if (!$this->bot && $this->botId) {
				$this->bot = PlatformBot::dao()->getById($this->botId);
			}
			
			return $this->bot;
		}
		
		public function getBotId()
		{
			return $this->bot
				? $this->bot->getId()
				: $this->botId;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function setBot(PlatformBot $bot = null)
		{
			$this->bot = $bot;
			$this->botId = $bot ? $bot->getId() : null;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function setBotId($id = null)
		{
			$this->bot = null;
			$this->botId = $id;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function dropBot()
		{
			$this->bot = null;
			$this->botId = null;
			
			return $this;
		}
		
		/**
		 * @return PlatformCrawlTime
		**/
		public function getCrawTime()
		{
			if (!$this->crawTime && $this->crawTimeId) {
				$this->crawTime = PlatformCrawlTime::dao()->getById($this->crawTimeId);
			}
			
			return $this->crawTime;
		}
		
		public function getCrawTimeId()
		{
			return $this->crawTime
				? $this->crawTime->getId()
				: $this->crawTimeId;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function setCrawTime(PlatformCrawlTime $crawTime = null)
		{
			$this->crawTime = $crawTime;
			$this->crawTimeId = $crawTime ? $crawTime->getId() : null;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function setCrawTimeId($id = null)
		{
			$this->crawTime = null;
			$this->crawTimeId = $id;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function dropCrawTime()
		{
			$this->crawTime = null;
			$this->crawTimeId = null;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTimeStatus
		**/
		public function getStatus()
		{
			if (!$this->status && $this->statusId) {
				$this->status = new PlatformVisitedPageCrawlTimeStatus($this->statusId);
			}
			
			return $this->status;
		}
		
		public function getStatusId()
		{
			return $this->status
				? $this->status->getId()
				: $this->statusId;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function setStatus(PlatformVisitedPageCrawlTimeStatus $status = null)
		{
			$this->status = $status;
			$this->statusId = $status ? $status->getId() : null;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function setStatusId($id = null)
		{
			$this->status = null;
			$this->statusId = $id;
			
			return $this;
		}
		
		/**
		 * @return PlatformVisitedPageCrawlTime
		**/
		public function dropStatus()
		{
			$this->status = null;
			$this->statusId = null;
			
			return $this;
		}
	}
?>